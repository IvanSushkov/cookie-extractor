function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob)
        window.navigator.msSaveOrOpenBlob(file, filename);
    else {
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

function cookies_handler(domain, cookies){
    console.log(JSON.stringify(cookies));
    results = []
    cookies.forEach(function(cookie) {
        results.push({
            comment: null,
            domain: cookie.domain,
            name: cookie.name,
            expires: parseInt(cookie.expirationDate),
            value: cookie.value,
            version: 0,
            rfc2109: false,
            discard: true,
            path: cookie.path,
            port: null,
            comment_url: null,
            secure: cookie.secure
        })
    });
    download(JSON.stringify(results), domain + '.cookie', 'text/plain');
}

var get_cookies = function(domain){
    chrome.cookies.getAll({"domain": domain}, cookies => cookies_handler(domain, cookies));
}

function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function get_file_content(url, sendResponse){
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'blob';
    request.onload = function() {
        var reader = new FileReader();
        reader.readAsDataURL(request.response);
        reader.onload =  function(e){
            var b64_encoded = e.target.result.split(',')[1];
            var text = b64DecodeUnicode(b64_encoded);
            sendResponse({
                content: text
            });
        };
    };
    request.send();
}

var load = function(path, sendResponse) {
    var resourse_path = chrome.runtime.getURL(path);
    get_file_content(resourse_path, sendResponse)
}

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse){
        if(request.msg == "get_cookies") get_cookies(request.data);
        if(request.msg == "load") {
            load(request.file_path, sendResponse);
            return true;
        }
    }
);
