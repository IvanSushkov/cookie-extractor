 window.onload = function() {
     render_canvas();
 };

function render_canvas() {

    chrome.extension.sendMessage({msg: "load", file_path: "frame/panel.css.html"},
    function(response){
        var link = document.getElementsByTagName("link")[0];
        link.insertAdjacentHTML('beforebegin', response.content);
    });

    chrome.extension.sendMessage({msg: "load", file_path: "frame/panel.html"},
    function(response){
        var first_body_cld = document.body;
        first_body_cld.insertAdjacentHTML('afterBegin', response.content);

        var button = document.getElementById('button-export');
        button.addEventListener('click', function(){
            chrome.extension.sendMessage({ msg: "get_cookies", data: document.domain});
        });
    });
}
